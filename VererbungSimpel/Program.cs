﻿using System;

namespace VererbungSimpel
{
    class Program
    {
        static void Main(string[] args) {
            AuftragBasis[] auftraege =
            {
                new Auftrag("Frühstücken"),
                new Spezialauftrag("Arbeit beginnen"),
            };

            Console.WriteLine("\n***** " + (Console.Title = "Vererbung Simpel") + " *****\n");

            foreach (Auftrag auftrag in auftraege) {
                auftrag.Anzeigen();
            }

            Console.WriteLine("\nfertig.");
            Console.ReadLine();
        }
    }

    public abstract class AuftragBasis
    {
        protected string _text;

        protected AuftragBasis(string text)
        {
            _text = text;
        }

        public abstract void Anzeigen();
    }

    public class Auftrag : AuftragBasis
    {
        public Auftrag(string text) : base(text)
        {
        }

        public override void Anzeigen()
        {
            Console.WriteLine("Auftrag: " + _text);
        }
    }

    public class Spezialauftrag : Auftrag
    {
        private DateTime time;

        public Spezialauftrag(string text) : base(text) {
            _text = text;
            this.time = DateTime.Now;
        }

        public override void Anzeigen() {
            Console.WriteLine("Spezialauftrag " + time.ToLongTimeString() + ": " + _text);
        }
    }
}

