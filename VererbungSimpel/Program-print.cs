﻿class Program
{
  static void Main(string[] args) {
    Auftrag[] auftraege =
    {
      new Auftrag("Frühstücken"),
      new Spezialauftrag("Arbeit beginnen"),
    };

    Console.WriteLine("\n***** " + (Console.Title = "Vererbung Simpel") + " *****\n");

    foreach (Auftrag auftrag in auftraege) {
      auftrag.Anzeigen();
    }

    Console.WriteLine("\nfertig.");
    Console.ReadLine();
  }
}

public class Auftrag
{
  private string text;

  public Auftrag(string text) {
    this.text = text;
  }

  public void Anzeigen() {
    Console.WriteLine("Auftrag: " + text);
  }
}

public class Spezialauftrag : Auftrag
{
  private string text;
  private DateTime time;

  public Spezialauftrag(string text) : base(text) {
    this.text = text;
    this.time = DateTime.Now;
  }

  public void Anzeigen() {
    Console.WriteLine("Spezialauftrag " + time.ToLongTimeString() + ": " + text);
  }
}


